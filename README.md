[![build status](https://gitlab.com/Gluttton/BronKerbosch/badges/master/pipeline.svg)](https://gitlab.com/Gluttton/BronKerbosch/commits/master)
[![coverage report](https://gitlab.com/Gluttton/BronKerbosch/badges/master/coverage.svg)](https://gitlab.com/Gluttton/BronKerbosch/commits/master)


BronKerbosch
============

![Example graph from Wikipedia](http://upload.wikimedia.org/wikipedia/commons/thumb/5/5b/6n-graf.svg/240px-6n-graf.svg.png)


Lightweight implementation of the [Bron-Kerbosch algorithm](https://en.wikipedia.org/wiki/Bron%E2%80%93Kerbosch_algorithm) (pivoting version):  



```
BronKerbosch (R, P, X):
    if P and X are both empty:
        return R as a maximal clique
    choose a pivot vertex u in P ⋃ X
    for each vertex v in P \ N(u) do
        BronKerbosch (R ⋃ {v}, P ⋂ N(v), X ⋂ N(v) )
        P := P \ {v}
        X := X ⋃ {v}
```


Licence
=======
The library is provided under MIT license.


Requirements
============
C++11 is required. Solution has been tested on GNU/Linux system with GCC compiler, but it is expected that the solution is cross-platform.


The main features of the library
================================
Lightweight:  
- header-only solution;  
- short and clear implementation (except one trick with ```act``` function) which is very similar with definition on pseudocode:  

```
template <typename T>
void solve (Graph <T> R, Graph <T> P, Graph <T> X, std::function <void (Graph <T>, Graph <T>, Graph <T>)> act)
{
    if (P.empty () && X.empty () ) {
        act (R, P, X);
        return;
    }

    const auto u = pivot (P, X);
    auto Pu = P;
    Pu.remove_if ([& u](const Vertex <T> & p){return u.ns.count (p.id);});

    for (; !Pu.empty (); Pu.pop_front () ) {
        auto Ri = R;
        auto Pi = P;
        auto Xi = X;
        const auto v = Pu.front ();
        Ri.push_front (v);
        Pi.remove_if ([& v](const Vertex <T> & p){return !v.ns.count (p.id);});
        Xi.remove_if ([& v](const Vertex <T> & x){return !v.ns.count (x.id);});
        solve <T> (Ri, Pi, Xi, act);
        P.remove (v);
        X.push_front (v);
    }
}
```


Reliable:  
- despite that the solution doesn't contain restriction, assertions and validation it provided with unit-tests which prove correctness of logic and if launched with profiler then correctness of implementation;  
- the library is hosted with CI support which allows continuously check its state.


Handy:  
- the library provided with utils which make usage simple;  
    - to construct graphs is provided `edge` function;  
    - to construct complement graph is provided `complement` function.  
- the library contains a parametrized ```Vertex``` class which allows extend existed code without modifications;  
- the solution contains unit-tests which also are examples of usage.


Flexible:  
- this is appropriate tool for solving a bunch of problems:  
    - finding a single maximal clique;  
    - finding all maximal cliques;  
    - finding a maximum clique;  
    - finding a maximum weight clique;  
    - finding a single maximal independent set;  
    - finding all maximal independent set;  
    - etc...  


Unluckily the library contains one trick which brings some difficulties: this is function pointer ```act```.
At the same time this trick provides flexibility. The act is the action which applied when algorithm find clique.
Wide range of problems which can be solved is reached by using specific ```act```. Here is examples of appropriate
```act``` implementation for solving different problems.

Finding a single maximal clique (throw an exception to immediate exit):  

```
template <typename T>
void printer (Graph <T> R, Graph <T> P, Graph <T>)
{
    std::cout << "[ ";
    for (const auto & v : R) {
        std::cout << v.id << " ";
    }
    std::cout << "]" << std::endl;

    throw 0;
};
```


Finding all maximal cliques:  

```
template <typename T> using Solution = std::forward_list <Graph <T> >;


template <typename T>
void aggregator (Solution <T> & solution, Graph <T> R, Graph <T> P, Graph <T>)
{
    solution.push_front (R);
};
```


Finding a maximum clique:  

```
template <typename T>
void selector (Graph <T> & solution, Graph <T> R, Graph <T> P, Graph <T>)
{
    if (std::distance (solution.begin (), solution.end () ) < std::distance (R.begin (), R.end () ) ) {
        solution = R;
    }
};
```

