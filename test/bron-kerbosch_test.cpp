#include <gtest/gtest.h>
#include <memory>
#include <bron-kerbosch/bron-kerbosch.h>
#include <bron-kerbosch/bron-kerbosch_utils.h>



using namespace BronKerbosch;
using namespace std::placeholders;
template <typename T> using Solution = std::forward_list <Graph <T> >;



TEST (GraphUtils, AddEdgeFailed)
{
    {
        // Arrange.
        Graph <std::string> G;

        // Act.
        const auto result = edge <std::string> (G, "A", "B");

        // Assert.
        EXPECT_FALSE (result);
    }

    {
        // Arrange.
        Graph <std::string> G;
        Vertex <std::string> A ("A");
        G.push_front (A);

        // Act.
        const auto result = edge <std::string> (G, "A", "B");

        // Assert.
        EXPECT_FALSE (result);
    }

    {
        // Arrange.
        Graph <std::string> G;
        Vertex <std::string> B ("B");
        G.push_front (B);

        // Act.
        const auto result = edge <std::string> (G, "A", "B");

        // Assert.
        EXPECT_FALSE (result);
    }
}



TEST (GraphUtils, AddEdgeSuccess)
{
    // Arrange.
    Graph <std::string> G;
    Vertex <std::string> A ("A");
    Vertex <std::string> B ("B");
    G.push_front (A);
    G.push_front (B);

    // Act.
    const auto result = edge <std::string> (G, "A", "B");

    // Assert.
    EXPECT_TRUE (result);
    ASSERT_EQ (2, std::distance (G.begin (), G.end () ) );
    ASSERT_NE (G.end (), std::find (G.begin (), G.end (), A) );
    ASSERT_NE (G.end (), std::find (G.begin (), G.end (), B) );
    ASSERT_EQ (1, std::find (G.begin (), G.end (), A)->ns.count ("B") );
    ASSERT_EQ (1, std::find (G.begin (), G.end (), B)->ns.count ("A") );
}



TEST (GraphUtils, ComplementCase001)
{
    // Arrange.
    Graph <char> G;
    G.push_front ({'A'});

    // Act.
    const Graph <char> H = complement (G);

    // Assert.
    ASSERT_EQ (1, std::distance (H.begin (), H.end () ) );
    ASSERT_EQ (0, std::distance (H.front ().ns.begin (), H.front ().ns.end () ) );
}



TEST (GraphUtils, ComplementCase002)
{
    // Arrange.
    Graph <char> G;
    G.push_front ({'A'});
    G.push_front ({'B'});

    // Act.
    const Graph <char> H = complement (G);

    // Assert.
    ASSERT_EQ (2, std::distance (H.begin (), H.end () ) );
    const auto a = std::find_if (H.begin (), H.end (), [](const Vertex <char> & x){return x.id == 'A';});
    ASSERT_NE (H.end (), a);
    ASSERT_EQ (1, std::distance (a->ns.begin (), a->ns.end () ) );
    EXPECT_EQ (1, a->ns.count ('B') );
    const auto b = std::find_if (H.begin (), H.end (), [](const Vertex <char> & x){return x.id == 'B';});
    ASSERT_NE (H.end (), b);
    ASSERT_EQ (1, std::distance (b->ns.begin (), b->ns.end () ) );
    EXPECT_EQ (1, b->ns.count ('A') );
}



TEST (GraphUtils, ComplementCase003)
{
    // Arrange.
    Graph <char> G;
    G.push_front ({'A'});
    G.push_front ({'B'});
    edge <char> (G, 'A', 'B');

    // Act.
    const Graph <char> H = complement (G);

    // Assert.
    ASSERT_EQ (2, std::distance (H.begin (), H.end () ) );
    const auto a = std::find_if (H.begin (), H.end (), [](const Vertex <char> & x){return x.id == 'A';});
    ASSERT_NE (H.end (), a);
    ASSERT_EQ (0, std::distance (a->ns.begin (), a->ns.end () ) );
    const auto b = std::find_if (H.begin (), H.end (), [](const Vertex <char> & x){return x.id == 'B';});
    ASSERT_NE (H.end (), b);
    ASSERT_EQ (0, std::distance (a->ns.begin (), a->ns.end () ) );
}



TEST (GraphUtils, ComplementCase004)
{
    // Arrange.
    Graph <char> G;
    G.push_front ({'A'});
    G.push_front ({'B'});
    G.push_front ({'C'});

    // Act.
    const Graph <char> H = complement (G);

    // Assert.
    ASSERT_EQ (3, std::distance (H.begin (), H.end () ) );
    const auto a = std::find_if (H.begin (), H.end (), [](const Vertex <char> & x){return x.id == 'A';});
    ASSERT_NE (H.end (), a);
    ASSERT_EQ (2, std::distance (a->ns.begin (), a->ns.end () ) );
    ASSERT_NE (a->ns.end (), std::find (a->ns.begin (), a->ns.end (), 'B') );
    ASSERT_NE (a->ns.end (), std::find (a->ns.begin (), a->ns.end (), 'C') );
    const auto b = std::find_if (H.begin (), H.end (), [](const Vertex <char> & x){return x.id == 'B';});
    ASSERT_NE (H.end (), b);
    ASSERT_EQ (2, std::distance (b->ns.begin (), b->ns.end () ) );
    ASSERT_NE (b->ns.end (), std::find (b->ns.begin (), b->ns.end (), 'A') );
    ASSERT_NE (b->ns.end (), std::find (b->ns.begin (), b->ns.end (), 'C') );
    const auto c = std::find_if (H.begin (), H.end (), [](const Vertex <char> & x){return x.id == 'C';});
    ASSERT_NE (H.end (), c);
    ASSERT_EQ (2, std::distance (c->ns.begin (), c->ns.end () ) );
    ASSERT_NE (c->ns.end (), std::find (c->ns.begin (), c->ns.end (), 'A') );
    ASSERT_NE (c->ns.end (), std::find (c->ns.begin (), c->ns.end (), 'B') );
}



TEST (GraphUtils, ComplementCase005)
{
    // Arrange.
    Graph <char> G;
    G.push_front ({'A'});
    G.push_front ({'B'});
    G.push_front ({'C'});
    edge <char> (G, 'A', 'B');

    // Act.
    const Graph <char> H = complement (G);

    // Assert.
    ASSERT_EQ (3, std::distance (H.begin (), H.end () ) );
    const auto a = std::find_if (H.begin (), H.end (), [](const Vertex <char> & x){return x.id == 'A';});
    ASSERT_NE (H.end (), a);
    ASSERT_EQ (1, std::distance (a->ns.begin (), a->ns.end () ) );
    ASSERT_NE (a->ns.end (), std::find (a->ns.begin (), a->ns.end (), 'C') );
    const auto b = std::find_if (H.begin (), H.end (), [](const Vertex <char> & x){return x.id == 'B';});
    ASSERT_NE (H.end (), b);
    ASSERT_EQ (1, std::distance (b->ns.begin (), b->ns.end () ) );
    ASSERT_NE (b->ns.end (), std::find (b->ns.begin (), b->ns.end (), 'C') );
    const auto c = std::find_if (H.begin (), H.end (), [](const Vertex <char> & x){return x.id == 'C';});
    ASSERT_NE (H.end (), c);
    ASSERT_EQ (2, std::distance (c->ns.begin (), c->ns.end () ) );
    ASSERT_NE (c->ns.end (), std::find (c->ns.begin (), c->ns.end (), 'A') );
    ASSERT_NE (c->ns.end (), std::find (c->ns.begin (), c->ns.end (), 'B') );
}



TEST (GraphUtils, ComplementCase006)
{
    // Arrange.
    Graph <char> G;
    G.push_front ({'A'});
    G.push_front ({'B'});
    G.push_front ({'C'});
    edge <char> (G, 'A', 'B');
    edge <char> (G, 'A', 'C');

    // Act.
    const Graph <char> H = complement (G);

    // Assert.
    ASSERT_EQ (3, std::distance (H.begin (), H.end () ) );
    const auto a = std::find_if (H.begin (), H.end (), [](const Vertex <char> & x){return x.id == 'A';});
    ASSERT_NE (H.end (), a);
    ASSERT_EQ (0, std::distance (a->ns.begin (), a->ns.end () ) );
    const auto b = std::find_if (H.begin (), H.end (), [](const Vertex <char> & x){return x.id == 'B';});
    ASSERT_NE (H.end (), b);
    ASSERT_EQ (1, std::distance (b->ns.begin (), b->ns.end () ) );
    ASSERT_NE (b->ns.end (), std::find (b->ns.begin (), b->ns.end (), 'C') );
    const auto c = std::find_if (H.begin (), H.end (), [](const Vertex <char> & x){return x.id == 'C';});
    ASSERT_NE (H.end (), c);
    ASSERT_EQ (1, std::distance (c->ns.begin (), c->ns.end () ) );
    ASSERT_NE (c->ns.end (), std::find (c->ns.begin (), c->ns.end (), 'B') );
}



TEST (GraphUtils, ComplementCase007)
{
    // Arrange.
    Graph <char> G;
    G.push_front ({'A'});
    G.push_front ({'B'});
    G.push_front ({'C'});
    edge <char> (G, 'A', 'B');
    edge <char> (G, 'A', 'C');
    edge <char> (G, 'B', 'C');

    // Act.
    const Graph <char> H = complement (G);

    // Assert.
    ASSERT_EQ (3, std::distance (H.begin (), H.end () ) );
    const auto a = std::find_if (H.begin (), H.end (), [](const Vertex <char> & x){return x.id == 'A';});
    ASSERT_NE (H.end (), a);
    ASSERT_EQ (0, std::distance (a->ns.begin (), a->ns.end () ) );
    const auto b = std::find_if (H.begin (), H.end (), [](const Vertex <char> & x){return x.id == 'B';});
    ASSERT_NE (H.end (), b);
    ASSERT_EQ (0, std::distance (b->ns.begin (), b->ns.end () ) );
    const auto c = std::find_if (H.begin (), H.end (), [](const Vertex <char> & x){return x.id == 'C';});
    ASSERT_NE (H.end (), c);
    ASSERT_EQ (0, std::distance (c->ns.begin (), c->ns.end () ) );
}



TEST (BronKerbosch, Case001)
{
    // Arrange.
    Graph <std::string> P;
    P.push_front ({"A"});
    Solution <std::string> solution;
    const auto act = [& solution](Graph <std::string> R, Graph <std::string>, Graph <std::string>){solution.push_front (R);};

    // Act.
    solve <std::string> ({ {} }, P, { {} }, act);

    // Assert.
    ASSERT_EQ (1, std::distance (solution.begin (), solution.end () ) );
    ASSERT_NE (solution.end (), std::find_if (solution.begin (), solution.end (), [](const Clique <std::string> & s)->bool {return 1 == std::distance (s.begin (), s.end () );}) );
    {
        const auto clique = std::find_if (solution.begin (), solution.end (), [](const Clique <std::string> & s)->bool {return 1 == std::distance (s.begin (), s.end () );});
        EXPECT_NE (clique->end (), std::find (clique->begin (), clique->end (), std::string {"A"}) );
    }
}



TEST (BronKerbosch, Case002)
{
    // Arrange.
    Graph <std::string> P;
    P.push_front ({"A"});
    P.push_front ({"B"});
    edge <std::string> (P, "A", "B");
    Solution <std::string> solution;
    const auto act = [& solution](Graph <std::string> R, Graph <std::string>, Graph <std::string>){solution.push_front (R);};

    // Act.
    solve <std::string> ({ {} }, P, { {} }, act);

    // Assert.
    ASSERT_EQ (1, std::distance (solution.begin (), solution.end () ) );
    ASSERT_NE (solution.end (), std::find_if (solution.begin (), solution.end (), [](const Clique <std::string> & s)->bool {return 2 == std::distance (s.begin (), s.end () );}) );
    {
        const auto clique = std::find_if (solution.begin (), solution.end (), [](const Clique <std::string> & s)->bool {return 2 == std::distance (s.begin (), s.end () );});
        EXPECT_NE (clique->end (), std::find (clique->begin (), clique->end (), std::string {"A"}) );
        EXPECT_NE (clique->end (), std::find (clique->begin (), clique->end (), std::string {"B"}) );
    }
}



TEST (BronKerbosch, Case003)
{
    // Arrange.
    Graph <std::string> P;
    P.push_front ({"A"});
    P.push_front ({"B"});
    P.push_front ({"C"});
    P.push_front ({"D"});
    P.push_front ({"E"});
    P.push_front ({"F"});
    edge <std::string> (P, "A", "B");
    edge <std::string> (P, "A", "C");
    edge <std::string> (P, "A", "D");
    edge <std::string> (P, "B", "C");
    edge <std::string> (P, "B", "D");
    edge <std::string> (P, "B", "E");
    edge <std::string> (P, "B", "F");
    edge <std::string> (P, "C", "D");
    edge <std::string> (P, "C", "E");
    edge <std::string> (P, "C", "F");
    edge <std::string> (P, "D", "E");
    edge <std::string> (P, "D", "F");
    edge <std::string> (P, "E", "F");
    Solution <std::string> solution;
    const auto act = [& solution](Graph <std::string> R, Graph <std::string>, Graph <std::string>){solution.push_front (R);};

    // Act.
    solve <std::string> ({ {} }, P, { {} }, act);

    // Assert.
    ASSERT_EQ (2, std::distance (solution.begin (), solution.end () ) );
    ASSERT_NE (solution.end (), std::find_if (solution.begin (), solution.end (), [](const Clique <std::string> & s)->bool {return 4 == std::distance (s.begin (), s.end () );}) );
    ASSERT_NE (solution.end (), std::find_if (solution.begin (), solution.end (), [](const Clique <std::string> & s)->bool {return 5 == std::distance (s.begin (), s.end () );}) );
    {
        const auto clique = std::find_if (solution.begin (), solution.end (), [](const Clique <std::string> & s)->bool {return 4 == std::distance (s.begin (), s.end () );});
        EXPECT_NE (clique->end (), std::find (clique->begin (), clique->end (), std::string {"A"}) );
        EXPECT_NE (clique->end (), std::find (clique->begin (), clique->end (), std::string {"B"}) );
        EXPECT_NE (clique->end (), std::find (clique->begin (), clique->end (), std::string {"C"}) );
        EXPECT_NE (clique->end (), std::find (clique->begin (), clique->end (), std::string {"D"}) );
    }
    {
        const auto clique = std::find_if (solution.begin (), solution.end (), [](const Clique <std::string> & s)->bool {return 5 == std::distance (s.begin (), s.end () );});
        EXPECT_NE (clique->end (), std::find (clique->begin (), clique->end (), std::string {"B"}) );
        EXPECT_NE (clique->end (), std::find (clique->begin (), clique->end (), std::string {"C"}) );
        EXPECT_NE (clique->end (), std::find (clique->begin (), clique->end (), std::string {"D"}) );
        EXPECT_NE (clique->end (), std::find (clique->begin (), clique->end (), std::string {"E"}) );
        EXPECT_NE (clique->end (), std::find (clique->begin (), clique->end (), std::string {"F"}) );
    }
}



TEST (BronKerbosch, Case004)
{
    // Arrange.
    Graph <int> P;
    P.push_front ({4});
    P.push_front ({3});
    P.push_front ({2});
    P.push_front ({1});
    edge <int> (P, 1, 2);
    edge <int> (P, 1, 3);
    edge <int> (P, 2, 3);
    edge <int> (P, 2, 4);
    Solution <int> solution;
    const auto act = [& solution](Graph <int> R, Graph <int>, Graph <int>){solution.push_front (R);};

    // Act.
    solve <int> ({ {} }, P, { {} }, act);

    // Assert.
    ASSERT_EQ (2, std::distance (solution.begin (), solution.end () ) );
    ASSERT_NE (solution.end (), std::find_if (solution.begin (), solution.end (), [](const Clique <int> & s)->bool {return 2 == std::distance (s.begin (), s.end () );}) );
    ASSERT_NE (solution.end (), std::find_if (solution.begin (), solution.end (), [](const Clique <int> & s)->bool {return 3 == std::distance (s.begin (), s.end () );}) );
    {
        const auto clique = std::find_if (solution.begin (), solution.end (), [](const Clique <int> & s)->bool {return 2 == std::distance (s.begin (), s.end () );});
        EXPECT_NE (clique->end (), std::find (clique->begin (), clique->end (), 2) );
        EXPECT_NE (clique->end (), std::find (clique->begin (), clique->end (), 4) );
    }
    {
        const auto clique = std::find_if (solution.begin (), solution.end (), [](const Clique <int> & s)->bool {return 3 == std::distance (s.begin (), s.end () );});
        EXPECT_NE (clique->end (), std::find (clique->begin (), clique->end (), 1) );
        EXPECT_NE (clique->end (), std::find (clique->begin (), clique->end (), 2) );
        EXPECT_NE (clique->end (), std::find (clique->begin (), clique->end (), 3) );
    }
}



TEST (BronKerbosch, Case005)
{
    // Arrange.
    Graph <std::shared_ptr <int> > P;
    auto _1 = std::make_shared <int> (1);
    auto _2 = std::make_shared <int> (2);
    auto _3 = std::make_shared <int> (3);
    P.push_front ({_1});
    P.push_front ({_2});
    P.push_front ({_3});
    edge <std::shared_ptr <int> > (P, _1, _2);
    Solution <std::shared_ptr <int> > solution;
    const auto act = [& solution](Graph <std::shared_ptr <int> > R, Graph <std::shared_ptr <int> > P, Graph <std::shared_ptr <int> >){solution.push_front (R);};

    // Act.
    solve <std::shared_ptr <int> > ({ {} }, P, { {} }, act);

    // Assert.
    ASSERT_EQ (2, std::distance (solution.begin (), solution.end () ) );
    ASSERT_NE (solution.end (), std::find_if (solution.begin (), solution.end (), [](const Clique <std::shared_ptr <int> > & s)->bool {return 1 == std::distance (s.begin (), s.end () );}) );
    ASSERT_NE (solution.end (), std::find_if (solution.begin (), solution.end (), [](const Clique <std::shared_ptr <int> > & s)->bool {return 2 == std::distance (s.begin (), s.end () );}) );
    {
        const auto clique = std::find_if (solution.begin (), solution.end (), [](const Clique <std::shared_ptr <int> > & s)->bool {return 1 == std::distance (s.begin (), s.end () );});
        EXPECT_NE (clique->end (), std::find (clique->begin (), clique->end (), _3) );
    }
    {
        const auto clique = std::find_if (solution.begin (), solution.end (), [](const Clique <std::shared_ptr <int> > & s)->bool {return 2 == std::distance (s.begin (), s.end () );});
        EXPECT_NE (clique->end (), std::find (clique->begin (), clique->end (), _1) );
        EXPECT_NE (clique->end (), std::find (clique->begin (), clique->end (), _2) );
    }
}



template <typename T>
bool compare (const Graph <T> & a, const Graph <T> & b)
{
    const auto al = std::distance (a.begin (), a.end () );
    const auto bl = std::distance (b.begin (), b.end () );

    auto ai = a.begin ();
    auto bi = b.begin ();
    for (int i = 0; i < std::min (al, bl); ++i) {
        if (ai->id != bi->id) {
            return ai->id < bi->id;
        }
        std::advance (ai, 1);
        std::advance (bi, 1);
    }

    return al < bl;
}



namespace BronKerbosch
{

template <typename T>
inline bool operator != (const Vertex <T> & a, const Vertex <T> & b)
{
    return a.id != b.id;
}

}// namespace BranKerbosch



template <typename T>
void aggregator (Solution <T> & solution, Graph <T> R, Graph <T> P, Graph <T>)
{
    solution.push_front (R);
};



template <typename T>
void printer (Graph <T> R, Graph <T> P, Graph <T>)
{
    std::cout << "[ ";
    for (const auto & v : R) {
        std::cout << v.id << " ";
    }
    std::cout << "]" << std::endl;
};




TEST (BronKerbosch, Case006)
{
    // Arrange.
    Graph <std::string> P;
    P.push_front ({"A"});
    P.push_front ({"B"});
    Solution <std::string> etalon { { {"A"} }, { {"B"} } };
    Solution <std::string> solution;

    // Act.
    solve <std::string> ({ {} }, P, { {} }, std::bind (aggregator <std::string>, std::ref (solution), _1, _2, _3) );

    // Assert.
    ASSERT_EQ (std::distance (etalon.begin (), etalon.end () ), std::distance (solution.begin (), solution.end () ) );
    etalon.sort   (compare <std::string>);
    solution.sort (compare <std::string>);
    auto ei = etalon.begin   ();
    auto si = solution.begin ();
    for (int i = 0; i < std::distance (etalon.begin (), etalon.end () ); ++i) {
        EXPECT_EQ (* ei, * si);
        std::advance (ei, 1);
        std::advance (si, 1);
    }
}



TEST (BronKerbosch, Case007)
{
    // Arrange.
    Graph <int> P;
    P.push_front ({1});
    P.push_front ({2});
    edge <int> (P, 1, 2);
    Solution <int> etalon { { {1}, {2} } };
    Solution <int> solution;

    // Act.
    solve <int> ({ {} }, P, { {} }, std::bind (aggregator <int>, std::ref (solution), _1, _2, _3) );

    // Assert.
    ASSERT_EQ (std::distance (etalon.begin (), etalon.end () ), std::distance (solution.begin (), solution.end () ) );
    etalon.sort   (compare <int>);
    solution.sort (compare <int>);
    auto ei = etalon.begin   ();
    auto si = solution.begin ();
    for (int i = 0; i < std::distance (etalon.begin (), etalon.end () ); ++i) {
        EXPECT_EQ (* ei, * si);
        std::advance (ei, 1);
        std::advance (si, 1);
    }
}



TEST (BronKerbosch, Case008)
{
    // Arrange.
    Graph <int> P;
    P.push_front ({1});
    P.push_front ({2});
    P.push_front ({3});
    edge <int> (P, 1, 2);
    Solution <int> etalon { { {1}, {2} }, { {3} } };
    Solution <int> solution;

    // Act.
    solve <int> ({ {} }, P, { {} }, std::bind (aggregator <int>, std::ref (solution), _1, _2, _3) );

    // Assert.
    ASSERT_EQ (std::distance (etalon.begin (), etalon.end () ), std::distance (solution.begin (), solution.end () ) );
    etalon.sort   (compare <int>);
    solution.sort (compare <int>);
    auto ei = etalon.begin   ();
    auto si = solution.begin ();
    for (int i = 0; i < std::distance (etalon.begin (), etalon.end () ); ++i) {
        EXPECT_EQ (* ei, * si);
        std::advance (ei, 1);
        std::advance (si, 1);
    }
}



TEST (BronKerbosch, Case009)
{
    // Arrange.
    Graph <int> P;
    P.push_front ({1});
    P.push_front ({2});
    P.push_front ({3});
    P.push_front ({4});
    edge <int> (P, 1, 2);
    edge <int> (P, 1, 3);
    edge <int> (P, 2, 3);
    P = complement <int> (P);
    Solution <int> etalon { { {1}, {4} }, { {2}, {4} }, { {3}, {4} } };
    Solution <int> solution;

    // Act.
    solve <int> ({ {} }, P, { {} }, std::bind (aggregator <int>, std::ref (solution), _1, _2, _3) );

    // Assert.
    ASSERT_EQ (std::distance (etalon.begin (), etalon.end () ), std::distance (solution.begin (), solution.end () ) );
    etalon.sort   (compare <int>);
    solution.sort (compare <int>);
    auto ei = etalon.begin   ();
    auto si = solution.begin ();
    for (int i = 0; i < std::distance (etalon.begin (), etalon.end () ); ++i) {
        EXPECT_EQ (* ei, * si);
        std::advance (ei, 1);
        std::advance (si, 1);
    }
}



// This test is inspired by Dpynes@gmail.com.
// Find more info here:
// - https://towardsdatascience.com/graphs-paths-bron-kerbosch-maximal-cliques-e6cab843bc2c ;
// - https://github.com/DavidPynes/Tutorials/blob/master/Graphs/Graph_03/main.cpp .
TEST (BronKerbosch, Case010)
{
    // Arrange.
    Graph <std::string> P;
    P.push_front ({"Amy"});
    P.push_front ({"Jack"});
    P.push_front ({"Erin"});
    P.push_front ({"Sally"});
    P.push_front ({"Sue"});
    P.push_front ({"Max"});
    P.push_front ({"Jake"});
    P.push_front ({"Tom"});
    P.push_front ({"Lu"});
    P.push_front ({"Joe"});
    P.push_front ({"Ryan"});
    P.push_front ({"Jess"});
    P.push_front ({"Liz"});
    P.push_front ({"Ty"});
    P.push_front ({"Jay"});
    edge <std::string> (P, "Amy",   "Erin");
    edge <std::string> (P, "Amy",   "Jack");
    edge <std::string> (P, "Erin",  "Jack");
    edge <std::string> (P, "Erin",  "Sally");
    edge <std::string> (P, "Sally", "Sue");
    edge <std::string> (P, "Sally", "Max");
    edge <std::string> (P, "Max",   "Sue");
    edge <std::string> (P, "Sally", "Tom");
    edge <std::string> (P, "Sally", "Jake");
    edge <std::string> (P, "Tom",   "Jake");
    edge <std::string> (P, "Tom",   "Jess");
    edge <std::string> (P, "Tom",   "Lu");
    edge <std::string> (P, "Tom",   "Ryan");
    edge <std::string> (P, "Jess",  "Jake");
    edge <std::string> (P, "Jess",  "Lu");
    edge <std::string> (P, "Jess",  "Ryan");
    edge <std::string> (P, "Lu",    "Ryan");
    edge <std::string> (P, "Lu",    "Jake");
    edge <std::string> (P, "Lu",    "Joe");
    edge <std::string> (P, "Ryan",  "Jake");
    edge <std::string> (P, "Liz",   "Jay");
    edge <std::string> (P, "Liz",   "Ty");
    edge <std::string> (P, "Ty",    "Jay");
    Solution <std::string> solution;
    const auto act = [& solution](Graph <std::string> R, Graph <std::string>, Graph <std::string>){solution.push_front (R);};

    // Act.
    solve <std::string> ({ {} }, P, { {} }, act);

    // Assert. Helpers.
    const auto isAllVertexMatch = [&](const Graph <std::string> & g, const Graph <std::string> & vs)->bool {
        for (const auto & v : vs)
            if (g.end () == std::find (g.begin (), g.end (), v) )
                return false;
        return true;
    };
    const auto isGraphInSolution = [&](const Solution <std::string> & s, const Graph <std::string> & g)->bool {
        for (const auto & c : s)
            if (std::distance (c.begin (), c.end () ) == std::distance (g.begin (), g.end () ) )
                if (isAllVertexMatch (c, g) )
                    return true;
        return false;
    };

    // Assert. Tests.
    ASSERT_EQ (7, std::distance (solution.begin (), solution.end () ) );
    {
        Graph <std::string> P;
        P.push_front ({"Amy"});
        P.push_front ({"Jack"});
        P.push_front ({"Erin"});
        ASSERT_TRUE (isGraphInSolution (solution, P) );
    }
    {
        Graph <std::string> P;
        P.push_front ({"Erin"});
        P.push_front ({"Sally"});
        ASSERT_TRUE (isGraphInSolution (solution, P) );
    }
    {
        Graph <std::string> P;
        P.push_front ({"Sally"});
        P.push_front ({"Sue"});
        P.push_front ({"Max"});
        ASSERT_TRUE (isGraphInSolution (solution, P) );
    }
    {
        Graph <std::string> P;
        P.push_front ({"Sally"});
        P.push_front ({"Jake"});
        P.push_front ({"Tom"});
        ASSERT_TRUE (isGraphInSolution (solution, P) );
    }
    {
        Graph <std::string> P;
        P.push_front ({"Jake"});
        P.push_front ({"Tom"});
        P.push_front ({"Lu"});
        P.push_front ({"Ryan"});
        P.push_front ({"Jess"});
        ASSERT_TRUE (isGraphInSolution (solution, P) );
    }
    {
        Graph <std::string> P;
        P.push_front ({"Lu"});
        P.push_front ({"Joe"});
        ASSERT_TRUE (isGraphInSolution (solution, P) );
    }
    {
        Graph <std::string> P;
        P.push_front ({"Liz"});
        P.push_front ({"Ty"});
        P.push_front ({"Jay"});
        ASSERT_TRUE (isGraphInSolution (solution, P) );
    }

    #if 0
    for (const auto & g : solution) {
        for (const auto & v : g)
            std::cout << v.id << " ";
        std::cout << std::endl;
    }
    #endif
}
